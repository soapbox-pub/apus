defmodule Apus.TelnyxAdapter do
  @moduledoc """
  Apus adapter for Telnyx.
  https://developers.telnyx.com/docs/api/v2/overview
  """
  @behaviour Apus.Adapter
  @endpoint "https://api.telnyx.com/v2/messages"

  @impl Apus.Adapter
  @spec deliver(Apus.Message.t(), map()) :: Apus.Message.t() | {:error, any()}
  def deliver(message, config) do
    params = to_telnyx_params(message)

    case Tesla.post(@endpoint, params, headers: headers(config)) do
      {:ok, %{status: 200}} -> {:ok, message}
      error -> {:error, error}
    end
  end

  @impl Apus.Adapter
  @spec handle_config(map()) :: map()
  def handle_config(config), do: config

  @spec headers(map()) :: Keyword.t()
  defp headers(config) do
    [
      {"authorization", "Bearer #{config.auth_token}"},
      {"content-type", "application/json"}
    ]
  end

  @spec to_telnyx_params(Apus.Message.t()) :: body :: String.t()
  defp to_telnyx_params(%Apus.Message{} = message) do
    %{
      from: message.from,
      to: message.to,
      text: message.body
    }
    |> Jason.encode!()
  end
end
