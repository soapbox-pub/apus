defmodule Apus.TelnyxAdapterTest do
  use ExUnit.Case
  use ExVCR.Mock, adapter: ExVCR.Adapter.Hackney, options: [clear_mock: true]

  alias Apus.Message
  alias Apus.TelnyxAdapter

  describe "message delivery" do
    test "deliver/2 should deliver a message" do
      config = %{
        auth_token: "KEY017F1A3E267587A4B973275EB6E11C4B_iaEgJuOqroB66LPQMKd4Q4"
      }

      message = Message.new(from: "+15551234567", to: "+15557654321", body: "Hello from Telnyx!")

      use_cassette "telnyx_sms_from_success", match_requests_on: [:request_body] do
        {:ok, %Apus.Message{}} = TelnyxAdapter.deliver(message, config)
      end
    end
  end

  test "deliver/2 should return the telnyx error message" do
    config = %{
      auth_token: "KEY017F1A3E267587A4B973275EB6E11C4B_iaEgJuOqroB66LPQMKd4Q4"
    }

    message = Message.new(from: "+15551234568", to: "+15557654321", body: "Hello from Telnyx!")

    use_cassette "telnyx_sms_failure", match_requests_on: [:request_body] do
      {:error, _} = TelnyxAdapter.deliver(message, config)
    end
  end
end
